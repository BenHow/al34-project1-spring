package fr.afcepf.al34.project1.spring.springboutique.web;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

import org.primefaces.extensions.util.SavedEditableValueState;

import fr.afcepf.al34.project1.spring.springboutique.business.InventoryService;
import fr.afcepf.al34.project1.spring.springboutique.business.ProductService;
import fr.afcepf.al34.project1.spring.springboutique.entity.Inventory;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Type;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@ManagedBean
@Getter @Setter @NoArgsConstructor
@SessionScoped
public class StockMBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Product product;
	private Inventory inventory;
	private Type type;
	private List<Product> products;
	private List<Inventory> inventories;
	private int totalMinStock;
	private int totalQuantity;
	private String message;


	@Inject
	private InventoryService inventoryService;
	
	@Inject
	private ProductService productService;
	
	@PostConstruct
	public void init() {
		this.products = productService.getAllProducts();
//		this.inventories = productService.getAllInventories();
		
	}
	
	public int findTotalMinStock(Product product) {
		List<Inventory> inventories = product.getInventories();
		for(Inventory inventory : inventories) {
			totalMinStock=inventoryService.getTotalMinStock(inventory.getId()).getMinStock();
		}
		return totalMinStock;
	}
	
	public int findTotalQuatity(Product product) {
		List<Inventory> inventories = product.getInventories();
		for(Inventory inventory : inventories) {
			totalQuantity=inventoryService.getTotalQuantity(inventory.getId()).getQuantity();
		}
		return totalQuantity;
	}
//	public int calculateTotalQuantity(Product product) {
//		for (Inventory inventory : product.getInventories()) {
//			totalQuantity = totalQuantity + inventory.getQuantity();
//		}
//		return totalQuantity;
//
//	}
//	
//	public int calculateTotalMinStock(Product product) {
//		
//		for (Inventory inventory : product.getInventories()) {
//			totalMinStock = totalMinStock + inventory.getMinStock();
//		}
//		return totalMinStock;
//
//	}
	
	public String quantityReachedMinStock() {
		if (totalQuantity < totalMinStock) {}
			return message = "ATTENTION ! Le stock minimum est atteint pour ce produit";
	
	}
	
	public void modifyMinStock(Product product) {
		for (Inventory inventory : product.getInventories()) {
		inventory.setMinStock(totalMinStock);
		inventoryService.saveInBase(inventory);
		}
		product.setInventories(inventories);

		productService.saveInBase(product);
	}
}
