package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.ProductDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.PromotionDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;
import fr.afcepf.al34.project1.spring.springboutique.entity.Promotion;

@Service
@Transactional
public class PromotionServiceImpl implements PromotionService {

	
	
	@Autowired
	PromotionDao promotionDao;
	
	@Autowired
	ProductDao productDao;

	@Override
	public List<Promotion> getAllPromotions() {
		return (List<Promotion>) promotionDao.findAll();
	}

	@Override
	public List<Product> findWithPromotion() {
		return productDao.findByPromotionNotNull();
	}

	@Override
	public Promotion saveInBase(Promotion promo) {
		return promotionDao.save(promo);
	}

}
