package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "supplier")
public class Supplier implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

    private String name;

    private String siret;

    private String address;

    @OneToMany(mappedBy = "supplier")
    private List<Product> products = new ArrayList<Product> ();

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private City city;
    
    

}
