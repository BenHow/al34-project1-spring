package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import fr.afcepf.al34.project1.spring.springboutique.dao.CustomerAgeDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Category;
import fr.afcepf.al34.project1.spring.springboutique.entity.Composition;
import fr.afcepf.al34.project1.spring.springboutique.entity.CustomerAge;
import fr.afcepf.al34.project1.spring.springboutique.entity.Sport;

public interface CategoryService {

	CustomerAge saveInBase(CustomerAge ca);
	Sport saveInBase(Sport s);
	Category saveInBase(Category c);
	Composition saveInBase(Composition c);
	
	List<Sport> getAllSports();
	List<Category> getAllCategories();
	List<Composition> getAllCompositions();
	
}
