package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.BankCardDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.BatchDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.CardTypeDao;
import fr.afcepf.al34.project1.spring.springboutique.dao.CartDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.BankCard;
import fr.afcepf.al34.project1.spring.springboutique.entity.Batch;
import fr.afcepf.al34.project1.spring.springboutique.entity.CardType;
import fr.afcepf.al34.project1.spring.springboutique.entity.Cart;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Product;

@Service
@Transactional
public class CartServiceImpl implements CartService {

	@Autowired
	CartDao cartDao;
	
	@Autowired
	BatchDao batchDao;
	
	@Autowired
	CardTypeDao cardTypeDao;
	
	@Autowired
	BankCardDao bankCardDao;
	
	@Override
	public Cart saveInBase(Cart cart) {
		return cartDao.save(cart);
	}

	@Override
	public Cart findWithClient(Client client) {
		return cartDao.findByClient(client);
	}

	@Override
	public Batch saveInBase(Batch batch) {
		return batchDao.save(batch);
	}

	@Override
	public List<Product> findWithCart(Cart cart) {
		List<Product> result =  batchDao.findWithCart(cart);
		for (Product product : result) {
			product.getPhotos().size();
		}
		return batchDao.findWithCart(cart);
	}
	
	@Override
	public void removeFromCart(Batch batch) {
		batchDao.delete(batch);
	}

	@Override
	public CardType saveInBase(CardType type) {
		return cardTypeDao.save(type);
	}

	@Override
	public BankCard saveInBase(BankCard cb) {
		return bankCardDao.save(cb);
	}

	@Override
	public void removeCart(Cart cart) {
		cartDao.delete(cart);
	}

	@Override
	public void removeBatch(Batch batch) {
		batchDao.delete(batch);
		
	}

}
