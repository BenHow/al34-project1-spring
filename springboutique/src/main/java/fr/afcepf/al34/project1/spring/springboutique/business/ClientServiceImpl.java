package fr.afcepf.al34.project1.spring.springboutique.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.afcepf.al34.project1.spring.springboutique.dao.ClientDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.Client;
import fr.afcepf.al34.project1.spring.springboutique.entity.Credentials;

@Service
@Transactional
public class ClientServiceImpl implements ClientService {

	@Autowired
	ClientDao dao;
	
	@Override
	public Client searchById(Integer id) {
		return dao.findById(id).orElse(null);
	}

	@Override
	public Client saveInBase(Client client) {
		return dao.save(client);
	}

	@Override
	public Client findWithCredentials(Credentials credentials) {
		Client result = dao.findByCredentials(credentials);
		result.getBankCard().size();
		return result;
	}
	
	

}
