package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@DiscriminatorValue("User")
public class User implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "last_name")
    private String lastName;

	@Column(name = "first_name")
    private String firstName;

	@Column(name = "total_connection_time")
    private int totalConnectionTime;

    @OneToMany
    private List<Photo> photos = new ArrayList<Photo> ();

//    @Embedded
//    @AttributeOverrides({
//    @AttributeOverride(name = "montant", column = @Column(name = "montant")),
//    @AttributeOverride(name = "monnaie", column = @Column(name = "monnaie"))
//    }) 
    @OneToOne
    @JoinColumn(referencedColumnName = "id")
    private Credentials credentials;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private Gender gender;

}
