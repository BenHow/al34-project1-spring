package fr.afcepf.al34.project1.spring.springboutique.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import fr.afcepf.al34.project1.spring.springboutique.dao.SportProductDao;
import fr.afcepf.al34.project1.spring.springboutique.entity.SportProduct;


@Service
@Transactional
public class SportProductServiceImpl implements SportProductService {

	@Autowired
	SportProductDao sportProductDao;
	
	@Override
	public SportProduct saveInBase(SportProduct sportProduct) {
		return sportProductDao.save(sportProduct);
	}

}