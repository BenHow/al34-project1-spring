package fr.afcepf.al34.project1.spring.springboutique.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

//@NamedQueries({
//	@NamedQuery(name = "BankCard.findAll", query = "SELECT c FROM BankCard c"),
//	@NamedQuery(name = "BankCard.finByBankName", query = "SELECT c FROM BankCard c WHERE c.bankName = :bankName")
//	
//})

@Getter @Setter @NoArgsConstructor
@Entity
@Table(name = "bank_card")
public class BankCard implements Serializable{
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "bank_name")
    private String bankName;

    private String number;

    @Column(name = "expiration_date")
    private LocalDate expirationDate;

    @Column(name = "security_code")
    private int securityCode;

    @Column(name = "card_holder_name")
    private String cardHolderName;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private CardType cardType;

	public BankCard(String bankName, String number, LocalDate expirationDate, int securityCode, String cardHolderName,
			CardType cardType) {
		super();
		this.bankName = bankName;
		this.number = number;
		this.expirationDate = expirationDate;
		this.securityCode = securityCode;
		this.cardHolderName = cardHolderName;
		this.cardType = cardType;
	}
    
    
    

}
